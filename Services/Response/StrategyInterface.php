<?php

namespace App\Services\Response;

interface StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 业务行为策略接口 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    public function response($serv, $fd, $from_id, $data, $processData = []);

    public function getData($no);

}