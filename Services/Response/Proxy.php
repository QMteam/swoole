<?php

namespace App\Services\Response;

class Proxy implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 代理模式来实现主动推送消息给终端 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    public function getData($no)
    {

    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        return $serv->send($fd, $data);
    }
}
