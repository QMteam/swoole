<?php

namespace App\Services\Response;

use App\Algorithm\Pack;

class Error extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 接受设备上传的错误信息 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getData($no)
    {
        // TODO: Implement getData() method.
    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
//            'login_time' => time(),
            'fd' => $fd,
            'constantly_time' => time(),
            'has_error' => '1',
            //todo 错误码？？？ 及何时上报恢复正常
        ];
        $this->update($processData, $insertData, '设备主动上报错误');
        $respose = $this->getResponseData($processData, '81', '0759');
        return $serv->send($fd, Pack::get($respose));
    }
}
