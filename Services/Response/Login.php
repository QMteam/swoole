<?php

namespace App\Services\Response;

use App\Algorithm\Pack;
use App\Core\Log;

class Login extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 主动登录服务器 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    public function getData($no)
    {

    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
            'login_time' => time(),
            'fd' => $fd,
            'on_line' => '1',
            'constantly_time' => time(),
        ];
        Log::info('登录的数据', [$insertData]);
        $this->update($processData, $insertData, '登录');
        $return = $this->getResponseData($processData, '81', '0159');
        return $serv->send($fd, Pack::get($return));
    }
}
