<?php

namespace App\Services\Response;

use App\Algorithm\Pack;

class Constantly extends StrategyCommon implements StrategyInterface
{

    /*
    |--------------------------------------------------------------------------
    | 实时接受数据 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getData($no)
    {
        // TODO: Implement getData() method.
    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
//            'login_time' => time(),
            'fd' => $fd,
            'on_line' => '1',
            'constantly_time' => time(),
        ];
        $this->update($processData, $insertData, '心跳数据');

        $respose = $this->getResponseData($processData, '81', '0259');
        return $serv->send($fd, Pack::get($respose));
    }
}
