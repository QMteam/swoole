<?php

namespace App\Services\Response;

use App\Algorithm\Pack;

class ResponseClose extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 回复云端开具 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getData($no)
    {
        // TODO: Implement getData() method.
    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
            'fd' => $fd,
            'constantly_time' => time(),
        ];

        $this->update($processData, $insertData, '设备回复关局');
        $fd = Cache::get($processData['mac'] . '84');
        Cache::set($processData['mac'] . '84', null);

        return $serv->send($fd, Pack::get($processData['data']));
    }
}
