<?php

namespace App\Services\Response;

use App\Algorithm\Pack;

class ResponseBegin extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 回复云端开具 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getData($no)
    {
        // TODO: Implement getData() method.
    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
            'fd' => $fd,
            'constantly_time' => time(),
        ];
        $return = $processData['data'];
        switch ($return) {
            case '010a'://开具成功
                break;
            case '0201'://永久开具
                $insertData['open_count'] = (int)hexdec(substr($processData['data'], 0, 2));
                $insertData['open_surplus'] = (int)hexdec(substr($processData['data'], 2, 2));
        }


        $this->update($processData, $insertData, '设备回复开具');
        $fd = Cache::get($processData['mac'] . '83');
        Cache::set($processData['mac'] . '83', null);

        return $serv->send($fd, Pack::get($processData['data']));
    }
}
