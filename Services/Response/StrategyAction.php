<?php

namespace App\Services\Response;

class StrategyAction
{
    /*
    |--------------------------------------------------------------------------
    | 对外的策略模式 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    private $activeObj = null;

    public function __construct($actionObj)
    {
        $this->activeObj = $actionObj;
    }

    public function action($serv, $fd, $from_id, $data, $processData = [])
    {
        return $this->activeObj->response($serv, $fd, $from_id, $data, $processData);
    }
}
