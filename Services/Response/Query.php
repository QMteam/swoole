<?php

namespace App\Services\Response;

use App\Algorithm\Pack;
use App\Core\Cache;

class Query extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 设备上传开具信息 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getData($no)
    {
        // TODO: Implement getData() method.
    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
            'fd' => $fd,
            'constantly_time' => time(),
            'on_line' => '1',
        ];

        $one = substr($processData['data'], 0, 2);
        $two = substr($processData['data'], 2, 2);

        if ($one == '00') {
            //有故障
            $insertData['has_error'] = '1';
            $insertData['error_info'] = $two;
        } elseif ($one == '01') {
            $insertData['open_surplus'] = (int)hexdec($two);
//            'open_count' => (int)hexdec(substr($processData['data'], 0, 2)),
        }else if ($one == '02' && $two == '00') {
            //永久开局
            $insertData['open_surplus'] = 0;
            $insertData['open_surplus'] = 0;
        }

        $this->update($processData, $insertData, '设备响应查询');

        $fd = Cache::get($processData['mac'] . '82');
        Cache::set($processData['mac'] . '82', null);

        return $serv->send($fd, Pack::get($processData['data']));
    }
}
