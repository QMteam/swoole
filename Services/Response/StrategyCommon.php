<?php

namespace App\Services\Response;

use App\Algorithm\Crc;
use App\Core\Config;
use App\Core\Db;
use App\Core\Log;

class StrategyCommon
{
    /*
    |--------------------------------------------------------------------------
    | 策略的通用方法 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getResponseData($processData = [], $cmd, $data)
    {
        $config = Config::get('MAHJONG');
        $initCmdData = $config['params'];
        $cmdData = sprintf($initCmdData, '__', $processData['serverNo'], $cmd, $processData['mac']);
        $cmdData .= $data;
        $length = (strlen($cmdData) + 6) / 2;
        $lengthHex = dechex($length);
        $cmdData = str_replace('__', strlen($lengthHex) > 1 ? substr($lengthHex, -2) : '0' . $lengthHex, $cmdData);
        return $cmdData . Crc::get($cmdData);
    }

    public function update($processData, $insertData, $log = '更新数据库')
    {
        Log::info($log, $processData);
        $table = Config::get('TABLE');
        $filter = ['mac' => $processData['mac']];
        $count = Db::count($table, $filter);
        if ($count > 0) {
            Db::update($table, $insertData, $filter);
        } else {
            Db::insert($table, $insertData);
        }
        Log::info($log . '写入数据库执行SQL', [Db::log()]);
    }
}
