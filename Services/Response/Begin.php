<?php

namespace App\Services\Response;

use App\Algorithm\Pack;

class Begin extends StrategyCommon implements StrategyInterface
{

    /*
    |--------------------------------------------------------------------------
    | 设备上传开具信息 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    public function getData($no)
    {
        // TODO: Implement getData() method.
    }

    public function response($serv, $fd, $from_id, $data, $processData = [])
    {
        $insertData = [
            'mac' => $processData['mac'],
            'fd' => $fd,
            'constantly_time' => time(),
            'open_count' => (int)hexdec(substr($processData['data'], 0, 2)),
            'open_surplus' => (int)hexdec(substr($processData['data'], 2, 2)),
        ];
        $this->update($processData, $insertData, '设备上报开具数');
        $respose = $this->getResponseData($processData, '81', '0359');
        return $serv->send($fd, Pack::get($respose));
    }
}
