<?php

namespace App\Services;

use App\Algorithm\Common;
use App\Algorithm\Crc;
use App\Algorithm\Pack;
use App\Core\Cache;
use App\Core\Config;
use App\Core\Log;
use App\Services\Response\StrategyAction;

class Receive implements ServiceInterface
{
    /*
    |--------------------------------------------------------------------------
    | 接受终端数据并进行返回 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    private static $action = null;

    public static function exec($serv, $fd, $from_id, $data)
    {
        if ('proxy' == substr($data, 0, 5)) {
            $data = substr($data, 5);
            $processData = Common::processData($data);
            $mac = $processData['mac'];
            $proxyFd = Cache::get($mac);
            if ($proxyFd) {
                Cache::set($mac . $processData['cmd'], $fd); // 用来回复终端请求
                $serv->send($proxyFd, Pack::get($data));
            } else {
                Log::error('代理发送失败，fd不存在');
            }
            return true;
        }

        
        $data = bin2hex($data);
        //进行校验
        Log::info('得到的数据为', [$data]);
        if (false === Crc::check($data)) {
            Log::error('数据校验出错', [$data]);
            return false;
        }
        $processData = Common::processData($data);
        Log::info('处理过的数据', $processData);
        $config = Config::get('MAHJONG');
        if ($config['cmd'][$processData['cmd']]) {
            static::$action = new $config['cmd'][$processData['cmd']];
        }
        if (null === static::$action) {
            Log::error('行为不能为空');
            throw new \Exception('行为不能为空');
        }

        Cache::set($fd, $processData['mac']);
        Cache::set($processData['mac'], $fd);

        $actionObj = new StrategyAction(static::$action);
        $actionObj->action($serv, $fd, $from_id, $data, $processData);
    }
}
