<?php

namespace App\Services;

interface ServiceInterface
{
    /*
    |--------------------------------------------------------------------------
    | 提供服务接口 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public static function exec($serv, $fd, $from_id, $data);
}
