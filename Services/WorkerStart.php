<?php

namespace App\Services;

use App\Core\Config;
use App\Core\Db;
use App\Core\Log;

class WorkerStart implements ServiceInterface
{
    /*
    |--------------------------------------------------------------------------
    | 处理定时任务 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    |
    */
    public static function exec($serv, $fd, $from_id, $data)
    {
        //增加定时器 执行单位毫秒,当前100s
        $serv->tick(100000, function () use ($serv, $fd) {
            $timeOut = Config::get('TIME_OUT');
            $table = Config::get('TABLE');
            $data = Db::select($table, 'id', [
                'AND' => [
                    'constantly_time[<]' => time() - $timeOut,
                    'on_line' => '1',
                ],
                'LIMIT' => [0, 100],
                'ORDER' => ['constantly_time' => 'ASC'],
            ]);
            Log::info('定时任务执行的SQL', [Db::log()]);
            if ($data) {
                foreach ((array)$data as $v) {
                    $ret = Db::update($table, ['on_line' => '0', 'fd' => '-1'], ['id' => $v['id']]);
                    if (!$ret)
                        Log::error('定时任务更改在线状态失败', $v);
                }
            }
        });
    }
}
