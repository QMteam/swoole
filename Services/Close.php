<?php

namespace App\Services;

use App\Core\Cache;
use App\Core\Config;
use App\Core\Db;
use App\Core\Log;

class Close implements ServiceInterface
{
    /*
    |--------------------------------------------------------------------------
    | 断开tcp by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    |
    | 主要处理：清除$fd与卡信息在redis的关联
    |
    */
    public static function exec($serv, $fd, $from_id, $data)
    {
        $no = Cache::get($fd);
        Cache::del($fd);
        Cache::del($no);

        $table = Config::get('TABLE');
        $filter = ['fd' => $fd];
        $insertData = [
            'fd' => '-1',
            'on_line' => '0',
        ];
        $ret = Db::update($table, $insertData, $filter);

        if (!$ret)
            Log::error('断开连接更改数据库失败', $filter);
    }
}
