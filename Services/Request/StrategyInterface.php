<?php

namespace App\Services\Request;

interface StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 业务行为策略接口 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    public function getRequestData($mac, $data);

}