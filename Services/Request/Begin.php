<?php

namespace App\Services\Request;

use App\Algorithm\SerialNumber;


class Begin extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 平台开局 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getRequestData($mac, $data = '')
    {
        $cmd = '83';
        $data = dechex($data);
        $data = strlen($data) > 1 ? $data : '0' . $data;

        if (!$this->hasCache($mac, $cmd)) {
            throw new \Exception('上次请求为完成，请等待！');
        }

        $processData = [
            'mac' => SerialNumber::get($mac),
        ];
        return $this->requestData($processData, $cmd, $data);
    }

}
