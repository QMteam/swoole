<?php

namespace App\Services\Request;

use App\Core\Cache;

class StrategyCommon
{
    public function requestData($processData, $cmd, $data)
    {
        $serviceNo = (int)Cache::get('service_no');
        $serviceNo++;
        Cache::set('service_no', $serviceNo);
        $processData['serverNo'] = dechex($serviceNo);
        $processData['serverNo'] = strlen($processData['serverNo']) < 2 ? '0' . $processData['serverNo'] : $processData['serverNo'];
        $obj = new \App\Services\Response\StrategyCommon();
        return $obj->getResponseData($processData, $cmd, $data);
    }

    public function hasCache($mac, $cmd)
    {
        if (Cache::get($mac . $cmd)) {
            return true;
        }
        return false;
    }
}
