<?php

namespace App\Services\Request;

class StrategyAction
{
    /*
    |--------------------------------------------------------------------------
    | 对外的策略模式 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    private $activeObj = null;

    public function __construct($actionObj)
    {
        $this->activeObj = $actionObj;
    }

    public function action($mac, $args = '')
    {
        return $this->activeObj->getRequestData($mac, $args);
    }
}
