<?php

namespace App\Services\Request;

use App\Algorithm\SerialNumber;

class QueryStatus extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 平台主动查询 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getRequestData($mac, $data = '')
    {
        $cmd = '82';

        if (!$this->hasCache($mac, $cmd)) {
            throw new \Exception('上次请求为完成，请等待！');
        }

        $processData = [
            'mac' => SerialNumber::get($mac),
        ];
        return $this->requestData($processData, $cmd, $data);
    }
}
