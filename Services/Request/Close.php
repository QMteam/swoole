<?php

namespace App\Services\Request;

use App\Algorithm\SerialNumber;

class Close extends StrategyCommon implements StrategyInterface
{
    /*
    |--------------------------------------------------------------------------
    | 平台下发关局 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function getRequestData($mac, $data = '')
    {
        $cmd = '84';
        $data = dechex($data);

        if (!$this->hasCache($mac, $cmd)) {
            throw new \Exception('上次请求为完成，请等待！');
        }

        $processData = [
            'mac' => SerialNumber::get($mac),
        ];
        return $this->requestData($processData, $cmd, $data);
    }

}
