<?php

namespace App\Services;

use App\Core\Log;

class Connect implements ServiceInterface
{
    /*
    |--------------------------------------------------------------------------
    | tcp连接处理 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    |
    | redis写入关联等信息
    |
    */

    // 闭包函数，$serv, $fd, $from_id, $data是swoole丢过来的参数
    public static function exec($serv, $fd, $from_id, $data)
    {
        // TODO: 卡第一次连接到底会传啥。
        $data = bin2hex($data);
        Log::info('第一次连接的数据', [$data]);
    }
}
