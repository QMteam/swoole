<?php

/*
|--------------------------------------------------------------------------
| 启动服务入口 by lxpfigo QQ:563086127
|--------------------------------------------------------------------------
|
| 本框架依赖Swoole、Redis，请安装这两个扩展,php版本5.4以上（不含5.4）
| 开启服务必须在CLI模式下
|
*/

define('BASE_ROOT', realpath(__DIR__));

define('MAHJONG_REDIS_SEND_MESSAGE_KEY', '__mahjong_send_message_key__');

define('REDIS_PREFIX', '__mahjong_redis_prefix__');

require_once BASE_ROOT . '/Core/AutoLoad.php';

require_once BASE_ROOT . '/vendor/autoload.php';

spl_autoload_register('App\\Core\\AutoLoad::load');

class Mahjong
{

    public static function start($params = [])
    {
        try {
            // 执行基础的扩展检测，开启报错，检测数据表是否有建立
            \App\Core\Boot::init();

            $sapiType = php_sapi_name();

            if ('cli' == strtolower(substr($sapiType, 0, 3))) {
                return static::startService();
            }
        } catch (Exception $e) {
            echo '<pre>';
            var_dump($e->getMessage());
            exit;
        }

    }

    private static function startService()
    {
        \App\Core\Boot::init();
        return \App\Core\Boot::startService();
    }

    public static function send($type = '82', $mac, $agrs = '')
    {
        try {
            \App\Core\Boot::init();
            $config = \App\Core\Config::get('MAHJONG');
            $upload = $config['upload'];

            if (!$upload[$type])
                throw new \Exception('发送命令不存在');

            $action = new $upload[$type];

            $actionObj = new \App\Services\Request\StrategyAction($action);

            $data = $actionObj->action($mac, $agrs);

            $ret = \App\Core\Client::send($data);

            return ['errorCode' => 0, 'data' => $ret];

        } catch (Exception $e) {
            return ['errorCode' => 1, 'message' => $e->getMessage()];
        }
    }

}
