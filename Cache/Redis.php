<?php

namespace App\Cache;

use App\Core\Config;

class Redis implements CacheInterface
{
    /*
    |--------------------------------------------------------------------------
    | redis操作 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    private $_redis = null;

    public function __construct()
    {
        $config = Config::get('CACHE');
        $redisConfig = $config['redis'];
        $this->_redis = new \Redis();
        $this->_redis->connect($redisConfig['service'], $redisConfig['port']);

        if ($redisConfig['password']) {
            $this->_redis->auth($redisConfig['password']);
        }

        if (!$this->_redis->ping()) {
            throw new \Exception($this->_redis->getLastError());
        }

    }

    public function connect()
    {

    }

    public function get($key)
    {
        $key = REDIS_PREFIX . $key;

        return $this->_redis->get($key);
    }

    public function set($key, $value)
    {
        $key = REDIS_PREFIX . $key;
        return $this->_redis->set($key, $value);
    }

    public function del($key)
    {
        $key = REDIS_PREFIX . $key;
        return $this->_redis->del($key);
    }

    public function lPush($key, $value)
    {
        $key = REDIS_PREFIX . $key;
        return $this->_redis->lPush($key, $value);
    }

    public function rPop($key)
    {
        $key = REDIS_PREFIX . $key;
        return $this->_redis->rPop($key);
    }

    public function lSize($key)
    {
        $key = REDIS_PREFIX . $key;
        return $this->_redis->lSize($key);
    }
}
