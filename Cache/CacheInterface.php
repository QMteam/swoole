<?php

namespace App\Cache;

interface CacheInterface
{
    /*
    |--------------------------------------------------------------------------
    | 缓存统一接口 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public function connect();

    public function set($key, $value);

    public function get($key);

    public function del($key);

    public function lPush($key, $value);

    public function rPop($key);

    public function lSize($key);
}
