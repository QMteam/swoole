<?php

namespace App\Core;

class Debug
{
    /*
    |--------------------------------------------------------------------------
    | 是否显示错误信息 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public static function error()
    {
        if (true === Config::get('DEBUG')) {
            ini_set('display_errors', 'On');
            error_reporting(E_ALL ^ E_STRICT);
        }
    }
}
