<?php

namespace App\Core;

use App\Core\Swoole;
use App\Services\Close;
use App\Services\Connect;
use App\Services\Receive;

class Boot
{
    /*
    |--------------------------------------------------------------------------
    | 启动服务 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    public static function startService()
    {
        Swoole::connect('App\\Services\\Connect::exec');

        Swoole::workerStart('App\\Services\\WorkerStart::exec');

        Swoole::receive('App\\Services\\Receive::exec');

        Swoole::close('App\\Services\\Close::exec');

        Swoole::start();


    }

    public static function startClient()
    {
        TcpClent::connect('App\\Client\\Connect::exec');
        TcpClent::receive('App\\Client\\Receive::exec');
        TcpClent::error('App\\Client\\Error::exec');
        TcpClent::close('App\\Client\\Close::exec');

        TcpClent::start();

    }


    public static function init()
    {
        date_default_timezone_set('PRC');
        //判断是否加载了Redis或Swoole
        if (!extension_loaded('swoole') || !extension_loaded('redis'))
            throw new \Exception('框架依赖Swoole和Redis，请安装扩展');

        // 启动错误展示 ，级别：E_ALL E_STRICT
        Debug::error();
        //判断表是否存在
        $table = Config::get('TABLE');
        if (false === Db::tableExist($table)) {
            throw new \Exception('数据表不存在，请创建!');
        }
    }
}
