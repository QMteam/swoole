<?php

namespace App\Core;

class TcpClent extends Facades
{

    protected static $_instance = null;

    protected static $_client = null;

    static protected function getFacadeAccessor()
    {
        if (null === static::$_instance)
            static::$_instance = new TcpClent();

        return static::$_instance;
    }

    private function __construct()
    {
        static::$_client = new \swoole_client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_ASYNC);
    }

    protected function connect($con)
    {
        return static::$_client->on('connect', $con);
    }

    protected function receive($rec)
    {
        return static::$_client->on('receive', $rec);
    }

    protected function error($err)
    {
        return static::$_client->on('error', $err);
    }

    protected function close($clo)
    {
        return static::$_client->on('close', $clo);
    }

    protected function start()
    {
        $config = Config::get('SWOOLE');
        return static::$_client->connect($config['service'], $config['port']);
    }
}
