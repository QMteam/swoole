<?php

namespace App\Core;

use Medoo;

class Db
{

    /*
    |--------------------------------------------------------------------------
    | 一个简单的数据库操作类 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    |
    | 手册地址：http://medoo.lvtao.net/doc.php
    |
    */

    static private $_instance = null;

    static private $_db = null;

    private function __construct()
    {
        $config = Config::get('DB');
        static::$_db = new Medoo\Medoo($config);
    }


    public static function __callStatic($name, $arguments)
    {
        if (null === static::$_instance)
            static::$_instance = new self();

        return call_user_func_array([static::$_db, $name], $arguments);
    }
}
