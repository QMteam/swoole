<?php

namespace App\Core;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log extends Facades
{

    private static $log = null;
    private static $_instance = null;

    private function __construct()
    {
        static::$log = new Logger('mahjong');

        $file = BASE_ROOT . '/Data/Log/' . date('Ymd') . '.log';

        static::$log->pushHandler(new StreamHandler($file, Logger::DEBUG));
    }


    protected function warning($info, $content = [])
    {
        return static::$log->addWarning($info, $content);
    }

    protected function error($info, $content = [])
    {
        return static::$log->addError($info, $content);
    }

    protected function info($info, $content = [])
    {
        return static::$log->addInfo($info, $content);
    }


    static protected function getFacadeAccessor()
    {
        if (null === static::$_instance)
            static::$_instance = new Log();
        return static::$_instance;
    }

}
