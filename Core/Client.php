<?php

namespace App\Core;

class Client extends Facades
{
    /*
    |--------------------------------------------------------------------------
    | socket客户端，用来转发 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    static private $_instance = null;
    static private $_client = null;

    static protected function getFacadeAccessor()
    {
        if (null === static::$_instance)
            static::$_instance = new Client();
        return static::$_instance;
    }

    private function __construct()
    {
        static::$_client = new \swoole_client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_SYNC);
        $config = Config::get('SWOOLE');
        $res = static::$_client->connect($config['service'] ? $config['service'] : '127.0.0.1', $config['port'] ? $config['port'] : 4321);
        if (!$res) {
            throw new \Exception('Client Connect Error');
        }
    }

    protected function send($data)
    {
        static::$_client->send('proxy' . $data);
        $ret = bin2hex(static::$_client->recv(1024));
        static::$_client->close();
        return $ret ? $ret : '99';
    }
}
