<?php

namespace App\Core;

class Cache extends Facades
{
    private static $_instance;

    static protected function getFacadeAccessor()
    {
        if (!static::$_instance) {
            $config = Config::get('CACHE');
            $obj = $config['default'];
            static::$_instance = $obj ? new $obj : new \App\Cache\Redis();
        }

        return static::$_instance;
    }
}
