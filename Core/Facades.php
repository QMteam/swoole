<?php

namespace App\Core;

abstract class Facades
{

    public static function instance()
    {
        return static::getFacadeRoot();
    }

    public static function getFacadeRoot()
    {
        if (is_object($object = static::getFacadeAccessor())) {
            return $object;
        }
        throw new \Exception(sprintf('Facade:%s need getFacadeAccessor method return object', get_class()));
    }

    protected static function getFacadeAccessor()
    {
        throw new \Exception("Facade does not implement getFacadeAccessor method.");
    }


    public static function __callStatic($method, $args)
    {
        $instance = static::getFacadeRoot();

        return call_user_func_array([$instance, $method], $args);
    }
}
