<?php

namespace App\Core;

class Config
{
    /*
    |--------------------------------------------------------------------------
    | 获取配置文件 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    private static $_file = null;

    public static function get($key = null)
    {
        if (null === static::$_file) {
            $file = BASE_ROOT . '/Config/config.php';

            if (!file_exists($file)) {
                throw new \Exception('配置文件不存在！');
            }

            static::$_file = include $file;
        }

        if (!static::$_file) {
            return null;
        }

        return static::$_file[$key] ? static::$_file[$key] : static::$_file;
    }
}
