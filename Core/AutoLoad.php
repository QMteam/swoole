<?php

namespace App\Core;

class AutoLoad
{
    /*
    |--------------------------------------------------------------------------
    | 自动加载 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public static function load($class)
    {
        $class = str_replace('\\', '/', $class);

        $num = strpos($class, '/');
        if ('App' != substr($class, 0, $num)) {
            return true;//防止跟其他的自动加载类冲突，发生悲剧。。。。
        }

        $class = substr($class, $num);

        $file = BASE_ROOT . $class . '.php';

        if (!file_exists($file)) {
            throw new \Exception('文件：' . $file . '不存在');
        }

        return include $file;
    }

}
