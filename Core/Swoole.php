<?php

namespace App\Core;

class Swoole extends Facades
{
    /*
    |--------------------------------------------------------------------------
    | swoole启动tcp方法 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    static private $serv = null;

    static private $_instance = null;


    static protected function getFacadeAccessor()
    {
        if (null === static::$_instance)
            static::$_instance = new Swoole();

        return static::$_instance;
    }

    private function __construct()
    {
        $config = Config::get('SWOOLE');

        if (!$config || !is_numeric($config['port'])) {
            Log::error('配置文件端口不存在');
            throw new \Exception('端口号不存在');
        }

        static::$serv = new \swoole_server('0.0.0.0', $config['port']);

        if (!static::$serv) {
            Log::error('启动Swoole出错');
            throw new \Exception('启动Swoole出错!');
        }

        unset($config['port']);
        unset($config['service']);

        static::set($config);
    }

    private static function set($params = [])
    {
        static::$serv->set($params);
    }

    protected function connect($con)
    {
        static::$serv->on('connect', $con);
    }

    protected function workerStart($worker)
    {
        static::$serv->on('workerStart', $worker);
    }

    protected function receive($rec)
    {
        static::$serv->on('receive', $rec);
    }

    protected function close($clo)
    {
        static::$serv->on('close', $clo);
    }

    protected function start()
    {
        static::$serv->start();
    }
}
