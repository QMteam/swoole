<?php

namespace App\Algorithm;

use App\Core\Log;

class SerialNumber
{

    /*
    |--------------------------------------------------------------------------
    | 生成设备的序列号 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */

    static public function get($key)
    {
        $length = strlen($key);

        if (15 !== $length && 16 !== $length) {
            Log::error('卡号必须为15位或16', [$key]);
            throw new \Exception('卡号必须为15位或16');
        }

        if (0 === $length % 2) {
            return $key;
        }

        return $key . '0';
    }

    static public function _get($no)
    {
        return ltrim($no, '0');
    }
}
