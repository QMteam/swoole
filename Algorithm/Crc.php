<?php

namespace App\Algorithm;

use App\Core\Config;

class Crc
{
    /*
    |--------------------------------------------------------------------------
    | 数据校验 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    |对应C语言的算法如下：
        #include <stdio.h>
        int main()
        {
            char array[] = {0x43, 0x13, 0x00, 0x01, 0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x00, 0x00, 0x01, 0x00, 0x80, 0x0d};
            unsigned char check_sum = 0;
            int i = 0;
            for (i = 0; i < sizeof(array) / sizeof(char); i++) {
                check_sum += array[i];
            }
            printf("%x\n", check_sum);
            return 0;
        }
    */
    public static function get($data)
    {
        $array = str_split($data, 2);
        $count = 0;
        foreach ((array)$array as $key => $v) {
            $count += hexdec($v);
        }
        $retrun = dechex($count);

        $retrun = substr($retrun, -2);
        $retrun = strlen($retrun) > 1 ? $retrun : '0' . $retrun;
        $config = Config::get('MAHJONG');
        return isset($config['end']) ? $retrun . $config['end'] : $retrun . '5858';
    }

    public static function check($data)
    {
        $length = strlen($data);

        $protectedData = substr($data, 0, $length - 6);
        $sign = substr($data, $length - 6);

        return static::get($protectedData) == $sign;
    }
}
