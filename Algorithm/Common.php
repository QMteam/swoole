<?php

namespace App\Algorithm;

class Common
{
    /*
    |--------------------------------------------------------------------------
    | 通用计算规则 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    public static function hex2bin($str)
    {
        $hex = hexdec($str);

        $return = decbin($hex);
        $length = strlen($return);

        if ($length % 8 === 0)
            return $return;

        return str_repeat(0, 8 - $length % 8) . $return;

    }

    public static function processData($data)
    {
        //43 13 00 01 ff ee dd cc bb aa 00 00 01 00 80 0d e0 58 58
//        $length = substr($data, 2, 2);
        return [
            'serverNo' => substr($data, 4, 2),
            'mac' => substr($data, 8, 16),
            'cmd' => substr($data, 6, 2),
            'data' => substr($data, 24, strlen($data) - 24 - 6),
        ];
    }
}
