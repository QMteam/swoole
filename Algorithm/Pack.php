<?php

namespace App\Algorithm;

class Pack
{
    /*
    |--------------------------------------------------------------------------
    | 16进制 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    |
    */
    public static function get($key)
    {
        return pack('H*', $key);
    }
}
