DROP TABLE IF EXISTS `mj_mahjong_constantly`;

CREATE TABLE `mj_mahjong_constantly` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mac` varchar(16) DEFAULT NULL COMMENT '卡号',
  `login_time` int(10) DEFAULT NULL COMMENT '创建时间',
  `on_line` int(1) DEFAULT 1 COMMENT '是否在线',
  `fd` bigint(11) DEFAULT NULL COMMENT 'socket句柄',
  `has_error` int(1) DEFAULT 0 COMMENT '是否有故障',
  `error_info` varchar(200) DEFAULT NULL COMMENT '故障原因',
  `constantly_time` varchar(20) DEFAULT NULL COMMENT '实时时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_no` (`mac`),
  UNIQUE KEY `ind_fd` (`fd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
