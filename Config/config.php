<?php

/*
|--------------------------------------------------------------------------
| 配置文件 
|--------------------------------------------------------------------------
*/
return [
    'DEBUG' => true,
    'TIME_OUT' => 30,//单位秒 设备多久没发送心跳包到平台，平台将设备设为离线
    'CACHE' => [
        'default' => App\Cache\Redis::class,
        'redis' => [
            'service' => '127.0.0.1',
            'port' => '6379',
            'password' => '',
        ],
    ],
    'SWOOLE' => [
        'port' => 18888,
        'worker_num' => 1,
        'daemonize' => true,
        'backlog' => 128,
        'service' => 'serverhost',
    ],
    'MAHJONG' => [
        // 43 帧头
        // 13 字节数
        // 00 帧序号
        // 01 命令字（登录服务器）
        // ff ee dd cc bb aa 00 00 卡号
        // 01 00 版本号
        // 80 0d 软件版本
        // e0 校验
        // 58 58  帧尾
        'params' => '43%s%s%s%s',//todo 示例的硬件主次版本颠倒
        //1、字节数
        //2、帧序号
        //3、命令字
        //4、卡号
        'end' => '5858',
        'head' => '43',//todo 如果需要修改帧头不要忘记修改params
        'cmd' => [
            '01' => App\Services\Response\Login::class,//登录到服务器
            '02' => App\Services\Response\Constantly::class,//心跳包
            '03' => App\Services\Response\Begin::class,//设备上传开具
            '04' => App\Services\Response\Query::class,//设备响应平台查询
            '05' => App\Services\Response\ResponseBegin::class,//设备响应平台开局
            '06' => App\Services\Response\ResponseClose::class,//设备响应平台关局
            '07' => App\Services\Response\Error::class,//设备上传故障
        ],
        'upload' => [ // 平台下发命令
            '82' => App\Services\Request\QueryStatus::class,//查询设备状态
            '83' => App\Services\Request\Begin::class,//下发开具
            '84' => App\Services\Request\Close::class,//下发关局
        ],
    ],
    //数据库连接信息，如需要主从请参照 http://medoo.lvtao.net/doc.php 进行设置
    'DB' => [
        'database_type' => 'mysql',
        'database_name' => 'mahjong',
        'server' => 'localhost',
        'username' => 'mahjong',
        'password' => 'mahjong',
        'charset' => 'utf8',
        'prefix' => 'mj_',
    ],
    'TABLE' => 'mahjong_constantly',
];
