#include <stdio.h>
int main()
{
    char array[] = {0x43, 0x13, 0x00, 0x01, 0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x00, 0x00, 0x01, 0x00, 0x80, 0x0d};
    unsigned char check_sum = 0;
    int i = 0;
    for (i = 0; i < sizeof(array) / sizeof(char); i++) {
        check_sum += array[i];
    }
    printf("%x\n", check_sum);
    return 0;
}
