<?php

namespace App\Client;

use App\Core\Log;

class Connect implements ClientInterface
{
    /*
    |--------------------------------------------------------------------------
    | 发生连接 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    static public function exec($cli, $data = '')
    {
        Log::info('客户端启动成功', [$cli]);
    }
}
