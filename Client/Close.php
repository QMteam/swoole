<?php

namespace App\Client;

use App\Core\Log;

class Close implements ClientInterface
{
    /*
    |--------------------------------------------------------------------------
    | 关闭客户端 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    static public function exec($cli, $data = '')
    {
        Log::info('客户端关闭连接', [$cli]);
    }
}
