<?php

namespace App\Client;

use App\Core\Log;

class Error implements ClientInterface
{
    /*
    |--------------------------------------------------------------------------
    | 连接发生错误 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    static public function exec($cli, $data = '')
    {
        Log::error('客户端连接失败', [$cli]);
    }
}
