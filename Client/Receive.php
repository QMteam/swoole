<?php

namespace App\Client;

use App\Core\Cache;
use App\Core\Log;

class Receive implements ClientInterface
{
    /*
    |--------------------------------------------------------------------------
    | 处理接受到的数据 by lxpfigo QQ:563086127
    |--------------------------------------------------------------------------
    */
    static public function exec($cli, $data = '')
    {
        $maxWait = 10;
        $interval = 50;
        $index = 0;
        while (true) {
            $count = Cache::lSize(MAHJONG_REDIS_SEND_MESSAGE_KEY);
            if ($count > 0) {
                $index = 0;
                $data = Cache::rPop(MAHJONG_REDIS_SEND_MESSAGE_KEY);
                Log::info('客户端发送的消息：' . $data);
                $cli->send($data);
                continue;
            }

            usleep($interval * ($index % $maxWait));
            $index++;
        }
    }
}
