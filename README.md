### 框架使用说明

1、框架目录结构
- Algorithm 算法目录
> Common.php 通用算法  
> Crc.php 数据校验算法  
> Pack.php 16进制字符串转文件流  
> SerialNumber.php 获取mac
- Cache 缓存目录
> CacheInterface.php  
> Redis.php Redis实现类  
- Client Swoole客户端异步非堵塞服务，性能考虑启用，todo
> ClientInterface.php  
> Close.php  
> Connect.php  
> Error.php  
> Receive.php  
- composer.json composer文件（忽略）
- composer.lock composer文件（忽略）
- Config 配置文件夹
> config.php 配置文件
- Core 核心
>> AutoLoad.php 自动加载类实现  
>> Boot.php 启动入口  
>> Cache.php 对外缓存接口  
>> Client.php Swoole客户端启动  
>> Config.php 获取配置文件  
>> Db.php 获取数据库实例  
>> Debug.php 是否开始错误提示  
>> Facades.php 门脸类，主要用来实现将普通函数转为静态资源类来调用  
>> Log.php 日志记录  
>> Swoole.php Swoole TCP启动  
>> TcpClent.php Swoole 客户端启动  
- Data 日志文件目录，请确保有读写权限
>> Log  
>>> 20171008.log  
- DB.sql 数据库初始
- git.sh git钩子脚本（忽略）
- hooks.php git钩子脚本（忽略）
- Services 业务逻辑层
>> Close.php tcp连接断开执行逻辑  
>> Connect.php tcp连接开始逻辑  
>> Receive.php 处理消息逻辑（重点）  
>> Request 平台下发命令给终端处理逻辑  
>>> Begin.php 下发开局  
>>> Close.php 下发关局  
>>> QueryStatus.php 下发查询  
>>> StrategyAction.php  
>>> StrategyCommon.php  
>>> StrategyInterface.php  
>> Response 平台响应终端请求逻辑  
>>> Begin.php 开具  
>>> Constantly.php 心跳包  
>>> Error.php 错误  
>>> Login.php 登录  
>>> Proxy.php （无效）  
>>> Query.php 平台下发命令，终端返回后的查询  
>>> ResponseBegin.php 平台下发开局命令，终端返回后的查询  
>>> ResponseClose.php 平台下发关局命令，终端返回后的查询  
>>> StrategyAction.php  
>>> StrategyCommon.php  
>>> StrategyInterface.php  
>> ServiceInterface.php  
>> WorkerStart.php 定时任务逻辑，将设备未接到心跳包的设备设为离线 
- startClient.php
- start.php 框架入口
- startService.php 开启TCP服务 php startService.php
- vendor composer文件（忽略）
> autoload.php
>>catfan
>>> medoo  
>> composer  
>>> autoload_classmap.php  
>>> autoload_namespaces.php  
>>> autoload_psr4.php  
>>> autoload_real.php  
>>> autoload_static.php  
>>> ClassLoader.php  
>>> installed.json  
>>> LICENSE  
>> monolog  
>>> monolog  
>> psr  
>>> log  

2、启动TCP服务
  php startService.php
  注意：
    1.请确保Data目录有可读写权限
    2.服务器启动将会读取Config中的SWOOLE选项
    3.框架要求php版本5.4以上（不包括5.4）
    
3、平台下发指令给终端
    调用Mahjong类下的send方法即可， send方法会读取配置文件中的MAHJONG字段进行发送
    示例：
        查询设备状态
        require_once './start.php';
        Mahjong::send('82', 'ffeeddccbbaa0000');
    
4、业务实现
    Services文件夹下来实现所有的业务逻辑
    
5、扩展命令
   响应设备的命令：
    在Services\Response新建类文件，实现App\Services\Response\StrategyInterface 接口
    在Config文件中的cmd增加字段： '命令字段' => 指向类
    
   下发命令给设备:
     在Services\Request新建类文件，实现App\Services\Request\StrategyInterface 接口 
     在Config文件中的upload新增字段:'命令字段' => 指向类   
    